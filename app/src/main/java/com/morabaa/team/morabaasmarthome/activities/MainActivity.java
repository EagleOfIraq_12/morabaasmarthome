package com.morabaa.team.morabaasmarthome.activities;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static com.morabaa.team.morabaasmarthome.utils.SV.DEVICE_ID;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.adapters.FloorAdapter;
import com.morabaa.team.morabaasmarthome.interfaces.OnRoomClickedListener;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.Room;
import com.morabaa.team.morabaasmarthome.model.TcpMessage;
import com.morabaa.team.morabaasmarthome.tcp.TcpClient;
import com.morabaa.team.morabaasmarthome.tcp.TcpServer;
import com.morabaa.team.morabaasmarthome.utils.DB;
import com.morabaa.team.morabaasmarthome.utils.LocalData;
import com.morabaa.team.morabaasmarthome.utils.SV;
import com.morabaa.team.morabaasmarthome.utils.SV.TCP_COMMANDS.OTHER;
import com.morabaa.team.morabaasmarthome.utils.Utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements OnRoomClickedListener  /*NavigationView.OnNavigationItemSelectedListener */ {

    public static DB db;
    @SuppressLint("StaticFieldLeak")
    String floor;
    RecyclerView floorRecyclerView;
    FloorAdapter floorAdapter;

    TextView txtMainTemp;
    TextView txtMainHumidity;
    int tempsCount = 1;
    int tempsSum = 0;
    int humiditiessCount = 0;
    TcpServer.OnTcpMessageReceivedListener onTcpMessageReceivedListener = new TcpServer.OnTcpMessageReceivedListener() {
        @Override
        public void onTcpMessageReceived(TcpMessage tcpMessage) {
            String deviceString = tcpMessage.getBody();
            final Device device = new Gson()
                    .fromJson(deviceString, Device.class);

            if (tcpMessage.getDeviceId().equals(DEVICE_ID) || true) {
                switch (tcpMessage.getCommand()) {
                    case SV.TCP_COMMANDS.DEVICE.SET: {
                        switch ((int) device.getDeviceId()) {
                            case 30: {
                                if (device.getStatus() > 5
                                        && device.getStatus() < 50) {
                                    runOnUiThread(
                                            () -> {
                                                tempsSum = (tempsSum + device.getStatus());

                                                txtMainTemp.setText(tempsSum
                                                        / tempsCount + " ْ");
                                                tempsCount++;
                                            });
                                }
                                break;
                            }
                            case 33: {
                                if (device.getControllerId()
                                        == 117) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            txtMainHumidity
                                                    .setText(
                                                            device.getStatus()
                                                                    + " %");
                                        }
                                    });
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
    };
    private Button bntReset;
    private Button bntReboot;
    private int backCount = 0;

    public static List<Room> getRooms(final Context ctx) {
        db.roomDuo().deleteAll();

        db.roomDuo().insert(new Room() {{
            setId(115);
            setControllerId(114);
            setName("موقع 115");
            setIconName(Utils.getDrawableNameById(
                    ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(116);
            setControllerId(116);
            setName("موقع 116");
            setIconName(Utils.getDrawableNameById(
                    ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(117);
            setControllerId(117);
            setName("موقع 117");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(118);
            setControllerId(118);
            setName("موقع 118");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(119);
            setControllerId(119);
            setName("موقع 119");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(120);
            setControllerId(120);
            setName("موقع 120");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(121);
            setControllerId(121);
            setName("موقع 121");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(122);
            setControllerId(122);
            setName("موقع 122");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(123);
            setControllerId(123);
            setName("موقع 123");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(124);
            setControllerId(124);
            setName("موقع 124");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(125);
            setControllerId(125);
            setName("موقع 125");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(126);
            setControllerId(126);
            setName("موقع 126");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        return db.roomDuo().ROOMS();
    }

    public static List<Room> getRoomsX(final Context ctx) {
        db.roomDuo().deleteAll();

        db.roomDuo().insert(new Room() {{
            setId(117);
            setControllerId(117);
            setName("الصاله الرئيسيه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(118);
            setControllerId(126);
            setName("صالة التدريب");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(119);
            setControllerId(126);
            setName("المخزن");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(120);
            setControllerId(126);
            setName("غرفة الاداره");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(121);
            setControllerId(126);
            setName("مغاسل الارضي");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(122);
            setControllerId(121);
            setName("الاستراحه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(123);
            setControllerId(114);
            setName("الممر");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(124);
            setControllerId(114);
            setName("مغاسل الاول");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(125);
            setControllerId(112);
            setName("Embedded System");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(126);
            setControllerId(120);
            setName("غرفة المبرمجين");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        db.roomDuo().insert(new Room() {{
            setId(127);
            setControllerId(115);
            setName("صالة المبرمجين");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.room_bedroom_icon));
            setFloorId("1");
        }});

        return db.roomDuo().ROOMS();
    }

    public static List<Device> getDevices(final Context ctx) {
        db.deviceDuo().deleteAll();

        db.deviceDuo().insert(new Device() {{
            setId(115001);
            setDeviceId(1);
            setControllerId(115);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115002);
            setDeviceId(2);
            setControllerId(115);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115003);
            setDeviceId(3);
            setControllerId(115);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115004);
            setDeviceId(4);
            setControllerId(115);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115005);
            setDeviceId(5);
            setControllerId(115);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115006);
            setDeviceId(6);
            setControllerId(115);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(115);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(117006);
            setDeviceId(6);
            setControllerId(117);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117001);
            setDeviceId(1);
            setControllerId(117);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117002);
            setDeviceId(2);
            setControllerId(117);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117003);
            setDeviceId(3);
            setControllerId(117);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117004);
            setDeviceId(4);
            setControllerId(117);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117005);
            setDeviceId(5);
            setControllerId(117);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117006);
            setDeviceId(6);
            setControllerId(117);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(118001);
            setDeviceId(1);
            setControllerId(118);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118002);
            setDeviceId(2);
            setControllerId(118);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118003);
            setDeviceId(3);
            setControllerId(118);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118004);
            setDeviceId(4);
            setControllerId(118);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118005);
            setDeviceId(5);
            setControllerId(118);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118006);
            setDeviceId(6);
            setControllerId(118);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(119001);
            setDeviceId(1);
            setControllerId(119);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119002);
            setDeviceId(2);
            setControllerId(119);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119003);
            setDeviceId(3);
            setControllerId(119);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119004);
            setDeviceId(4);
            setControllerId(119);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119005);
            setDeviceId(5);
            setControllerId(119);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119006);
            setDeviceId(6);
            setControllerId(119);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(120001);
            setDeviceId(1);
            setControllerId(120);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120002);
            setDeviceId(2);
            setControllerId(120);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120003);
            setDeviceId(3);
            setControllerId(120);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120004);
            setDeviceId(4);
            setControllerId(120);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120005);
            setDeviceId(5);
            setControllerId(120);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120006);
            setDeviceId(6);
            setControllerId(120);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(121001);
            setDeviceId(1);
            setControllerId(121);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121002);
            setDeviceId(2);
            setControllerId(121);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121003);
            setDeviceId(3);
            setControllerId(121);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121004);
            setDeviceId(4);
            setControllerId(121);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121005);
            setDeviceId(5);
            setControllerId(121);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121006);
            setDeviceId(6);
            setControllerId(121);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(122001);
            setDeviceId(1);
            setControllerId(122);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122002);
            setDeviceId(2);
            setControllerId(122);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122003);
            setDeviceId(3);
            setControllerId(122);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122004);
            setDeviceId(4);
            setControllerId(122);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122005);
            setDeviceId(5);
            setControllerId(122);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(122006);
            setDeviceId(6);
            setControllerId(122);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(123001);
            setDeviceId(1);
            setControllerId(123);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123002);
            setDeviceId(2);
            setControllerId(123);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123003);
            setDeviceId(3);
            setControllerId(123);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123004);
            setDeviceId(4);
            setControllerId(123);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123005);
            setDeviceId(5);
            setControllerId(123);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(123006);
            setDeviceId(6);
            setControllerId(123);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(119001);
            setDeviceId(1);
            setControllerId(119);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119002);
            setDeviceId(2);
            setControllerId(119);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119003);
            setDeviceId(3);
            setControllerId(119);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119004);
            setDeviceId(4);
            setControllerId(119);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119005);
            setDeviceId(5);
            setControllerId(119);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(119006);
            setDeviceId(6);
            setControllerId(119);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(125001);
            setDeviceId(1);
            setControllerId(125);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125002);
            setDeviceId(2);
            setControllerId(125);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125003);
            setDeviceId(3);
            setControllerId(125);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125004);
            setDeviceId(4);
            setControllerId(125);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125005);
            setDeviceId(5);
            setControllerId(125);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(125006);
            setDeviceId(6);
            setControllerId(125);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(126001);
            setDeviceId(1);
            setControllerId(126);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126002);
            setDeviceId(2);
            setControllerId(126);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126003);
            setDeviceId(3);
            setControllerId(126);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126004);
            setDeviceId(4);
            setControllerId(126);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126005);
            setDeviceId(5);
            setControllerId(126);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126006);
            setDeviceId(6);
            setControllerId(126);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});

        db.deviceDuo().insert(new Device() {{
            setId(116001);
            setDeviceId(1);
            setControllerId(116);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116002);
            setDeviceId(2);
            setControllerId(116);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116003);
            setDeviceId(3);
            setControllerId(116);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116004);
            setDeviceId(4);
            setControllerId(116);
            setName("إضاءه 4");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116005);
            setDeviceId(5);
            setControllerId(116);
            setName("إضاءه 5");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(116006);
            setDeviceId(6);
            setControllerId(116);
            setName("إضاءه 6");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(116);
        }});

        StringBuilder s = new StringBuilder("[");
        for (Device device : db.deviceDuo().DEVICES()) {
            s.append(new Gson().toJson(device, Device.class)).append(",");

        }
        s.append("]");
        System.out.println("Devices ...  " + s);

        return db.deviceDuo().DEVICES();
    }

    public static void indoor(boolean b) {
        if (!b) {
//                  fromControllerMessageRef.addValueEventListener(fromControllerMessageListener);
        } else {
//                  fromControllerMessageRef.removeEventListener(fromControllerMessageListener);
        }
    }

    public static List<Device> getDevicesX(final Context ctx) {
        db.deviceDuo().deleteAll();

        //region room 117
        db.deviceDuo().insert(new Device() {{
            setId(118003);
            setDeviceId(3);
            setControllerId(118);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118004);
            setDeviceId(4);
            setControllerId(118);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118002);
            setDeviceId(2);
            setControllerId(118);
            setName("إضاءه 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118005);
            setDeviceId(5);
            setControllerId(118);
            setName("بلك سويج 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(118006);
            setDeviceId(6);
            setControllerId(118);
            setName("بلك سويج 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117003);
            setDeviceId(3);
            setControllerId(117);
            setName("المعرض");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(117006);
            setDeviceId(6);
            setControllerId(117);
            setName("بلك سويج 3");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(117);
        }});
        //endregion
        //region room 118
        db.deviceDuo().insert(new Device() {{
            setId(126002);
            setDeviceId(2);
            setControllerId(126);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126006);
            setDeviceId(6);
            setControllerId(126);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(118);
        }});
        //endregion
        //region room 119
        db.deviceDuo().insert(new Device() {{
            setId(119004);
            setDeviceId(4);
            setControllerId(126);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(119);
        }});
        //endregion
        //region room 120
        db.deviceDuo().insert(new Device() {{
            setId(119003);
            setDeviceId(3);
            setControllerId(119);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(126006);
            setDeviceId(6);
            setControllerId(126);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(120);
        }});
        //endregion
        //region room 121
        db.deviceDuo().insert(new Device() {{
            setId(126005);
            setDeviceId(5);
            setControllerId(126);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(121);
        }});

        //endregion

        //region room 122
        db.deviceDuo().insert(new Device() {{
            setId(121005);
            setDeviceId(5);
            setControllerId(121);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(121006);
            setDeviceId(6);
            setControllerId(121);
            setName("البصمه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(122);
        }});

        //endregion
        //region room 123
        db.deviceDuo().insert(new Device() {{
            setId(114004);
            setDeviceId(4);
            setControllerId(114);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(114005);
            setDeviceId(5);
            setControllerId(114);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115006);
            setDeviceId(6);
            setControllerId(115);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(123);
        }});

        //endregion
        //region room 124
        db.deviceDuo().insert(new Device() {{
            setId(114003);
            setDeviceId(3);
            setControllerId(114);
            setName("إضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(124);
        }});
        //endregion
        //region room 125
        db.deviceDuo().insert(new Device() {{
            setId(112003);
            setDeviceId(3);
            setControllerId(112);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(112004);
            setDeviceId(4);
            setControllerId(112);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(112006);
            setDeviceId(6);
            setControllerId(112);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(125);
        }});
        //endregion
        //region room 126
        db.deviceDuo().insert(new Device() {{
            setId(120002);
            setDeviceId(2);
            setControllerId(120);
            setName("إضاءه 1");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120003);
            setDeviceId(3);
            setControllerId(120);
            setName("إضاءه 2");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120005);
            setDeviceId(5);
            setControllerId(120);
            setName("بلك سويج يمين");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(120006);
            setDeviceId(6);
            setControllerId(120);
            setName("بلك سويج يسار");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(126);
        }});

        //endregion
        //region room 127
        db.deviceDuo().insert(new Device() {{
            setId(115002);
            setDeviceId(2);
            setControllerId(115);
            setName("اضاءه خارجيه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(127);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115003);
            setDeviceId(3);
            setControllerId(115);
            setName("اضاءه");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(127);
        }});
        db.deviceDuo().insert(new Device() {{
            setId(115005);
            setDeviceId(5);
            setControllerId(115);
            setName("بلك سويج");
            setIconName(Utils.getDrawableNameById(ctx, R.drawable.device_c));
            setRoomId(127);
        }});

        //endregion

        return db.deviceDuo().DEVICES();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        bntReset = findViewById(R.id.bntReset);
        bntReboot = findViewById(R.id.bntReboot);
        txtMainTemp = findViewById(R.id.txtMainTemp);
        txtMainHumidity = findViewById(R.id.txtMainHumidity);

        bntReboot.setOnClickListener((View view) ->
                TcpClient.send(new TcpMessage() {{
                    setCommand(OTHER.REBOOT);
                }})
        );
        bntReset.setOnClickListener((View view) ->
                TcpClient.send(new TcpMessage() {{
                    setCommand(OTHER.RESET);
                    setBody("");
                    setBodyClass("Other");
                }})
        );


//            System.out.println(
//                  "res 1 : "
//                        + exc("ifconfig wlan0 192.168.10.105 netmask 255.255.255.0 up"));
//
//            System.out.println(
//                  "res 2 : "
//                        + exc("route add default gw 192.168.10.1 dev wlan0"));

        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        setSupportActionBar(toolbar);
        setTitle("المنزل الذكي");
        db = android.arch.persistence.room.Room.databaseBuilder(
                MainActivity.this, DB.class,
                "smartHome.db"
        )
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        floorRecyclerView = findViewById(R.id.roomsGrid);

        ImageView btnFloorNo = findViewById(R.id.btnFloorNo);

        btnFloorNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                        MainActivity.this);

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.select_floor_layout,
                        null);
                dialogBuilder.setView(dialogView);

                String[] items = new String[]{"الاول", "الثاني"};

                final Spinner floorSpinner = dialogView.findViewById(R.id.floorSpinner);

                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                        MainActivity.this,
                        android.R.layout.simple_spinner_item,
                        items); //selected item will look like a spinner set from XML
                spinnerArrayAdapter.setDropDownViewResource(
                        android.R.layout.simple_spinner_dropdown_item);
                floorSpinner.setAdapter(spinnerArrayAdapter);
                final AlertDialog alertDialog = dialogBuilder.create();
                dialogView.findViewById(R.id.btnOk)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                floor = floorSpinner.getSelectedItem().toString();
                                Toast.makeText(MainActivity.this, floor,
                                        Toast.LENGTH_SHORT)
                                        .show();
                                alertDialog.dismiss();
                                setTitle("الطابق " + floor);
                            }
                        });

                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawable(

                        new ColorDrawable(
                                android.graphics.Color.TRANSPARENT
                        )
                );
                alertDialog.getWindow().setLayout(700, 600);

            }
        });
        initX(true);

        List<Room> rooms = MainActivity.db.roomDuo().ROOMS();
        floorAdapter = new FloorAdapter(MainActivity.this,
                rooms, "000");
        floorRecyclerView.setAdapter(floorAdapter);

        if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
            floorRecyclerView.setLayoutManager(

                    new GridLayoutManager(
                            MainActivity.this, 2)
            );
        } else {
            floorRecyclerView.setLayoutManager(
                    new GridLayoutManager(
                            MainActivity.this, 3)
            );
        }

        goOn();
//            DrawerLayout drawer = findViewById(R.id.drawer_layout);
//            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                  this, drawer, toolbar, R.string.navigation_drawer_open,
//                  R.string.navigation_drawer_close);
//            drawer.addDrawerListener(toggle);
//            toggle.syncState();

//            NavigationView navigationView = findViewById(R.id.nav_view);
//            navigationView.setNavigationItemSelectedListener(this);
    }

    private void initX(boolean x) {
        if (x) {
            if (db.roomDuo().count() < 1 || true) {
                getRoomsX(MainActivity.this);
            }
            if (db.deviceDuo().count() < 1 || true) {
                getDevicesX(MainActivity.this);
            }
        } else {
            if (db.roomDuo().count() < 1 || true) {
                getRooms(MainActivity.this);
            }
            if (db.deviceDuo().count() < 1 || true) {
                getDevices(MainActivity.this);
            }
        }
    }

    private void goOn() {
        SV.DEVICE_ID = LocalData.get(MainActivity.this, DEVICE_ID);
//            firebaseReference = database.getReference(LocalData.get(MainActivity.this, DEVICE_ID));
//            fromControllerMessageRef = firebaseReference.child("fromControllerMessage");
//            roomsRef = firebaseReference.child("locations");
//            devicesRef = firebaseReference.child("devices");
//            fromPhoneMessageRef = firebaseReference.child("fromPhoneMessageRef");
//
//            placeNameRef = firebaseReference.child("name");
//            roomsRef.addListenerForSingleValueEvent(roomsValueEventListener);
//            devicesRef.addListenerForSingleValueEvent(devicesValueEventListener);

        TcpServer tcpServer = new TcpServer();
        tcpServer.run();
        tcpServer.addOnTcpMessageReceivedListener(onTcpMessageReceivedListener);
//            indoor(true);
        // get sensors
        final Device device = new Device();
        device.setControllerId(117);
        device.setDeviceId(30);
        TcpClient.send(new TcpMessage() {{
            setCommand(SV.TCP_COMMANDS.DEVICE.GET);
            setBody(new Gson().toJson(device, Device.class));
            setBodyClass("Device");
        }});
        device.setDeviceId(31);
        TcpClient.send(new TcpMessage() {{
            setCommand(SV.TCP_COMMANDS.DEVICE.GET);
            setBody(new Gson().toJson(device, Device.class));
            setBodyClass("Device");
        }});
        device.setDeviceId(32);
        TcpClient.send(new TcpMessage() {{
            setCommand(SV.TCP_COMMANDS.DEVICE.GET);
            setBody(new Gson().toJson(device, Device.class));
            setBodyClass("Device");
        }});

    }

    @Override
    public void onBackPressed() {
//            DrawerLayout drawer = findViewById(R.id.drawer_layout);
//            if (drawer.isDrawerOpen(GravityCompat.START)) {
//                  drawer.closeDrawer(GravityCompat.START);
//            } else {
//            super.onBackPressed();
//            }
        if (backCount > 0) {
            super.onBackPressed();
            finish();
        } else {
            Toast.makeText(MainActivity.this, "اضغط مره اخرى للخروج.", Toast.LENGTH_SHORT)
                    .show();
            backCount++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backCount = 0;
                }
            }, 3000);
        }
    }

    @Override
    public void onRoomClicked(Room room, boolean b) {
        if (b) {
            for (Device device : db.deviceDuo().getDevicesByRoomId(room.getId())) {
                TcpMessage tcpMessage = new TcpMessage();
                tcpMessage.setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                device.setStatus(1);
                tcpMessage.setBody(new Gson().toJson(device, Device.class));
                tcpMessage.setBodyClass("Device");
                TcpClient.send(tcpMessage);
            }
        } else {
            for (Device device : db.deviceDuo().getDevicesByRoomId(room.getId())) {
                TcpMessage tcpMessage = new TcpMessage();
                tcpMessage.setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                device.setStatus(0);
                tcpMessage.setBody(new Gson().toJson(device, Device.class));
                tcpMessage.setBodyClass("Device");
                TcpClient.send(tcpMessage);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public String exc(String command) {
        StringBuilder output = new StringBuilder();
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }
//
//      @SuppressWarnings("StatementWithEmptyBody")
//      @Override
//      public boolean onNavigationItemSelected(MenuItem item) {
//            // Handle navigation view item clicks here.
//            int id = item.getItemId();
//
//            if (id == R.id.nav_camera) {
//                  // Handle the camera action
//            } else if (id == R.id.nav_gallery) {
//
//            } else if (id == R.id.nav_slideshow) {
//
//            } else if (id == R.id.nav_manage) {
//
//            } else if (id == R.id.nav_share) {
//
//            } else if (id == R.id.nav_send) {
//
//            }
//
//            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//            drawer.closeDrawer(GravityCompat.START);
//            return true;
//      }
}
