package com.morabaa.team.morabaasmarthome.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.TcpMessage;
import com.morabaa.team.morabaasmarthome.tcp.TcpClientAbstract;
import com.morabaa.team.morabaasmarthome.utils.SV;
import com.wang.avi.AVLoadingIndicatorView;
import java.util.ArrayList;
import java.util.List;

/**
 * -Created by eagle on 10/21/2017.
 */


public class RoomAdapter extends RecyclerView.Adapter<RoomViewHolder> {
      
      private AdapterActivityBridge adapterActivityBridge;
      private List<Device> devices;
      private Context ctx;
      private ColorMatrixColorFilter whiteFilter;
      private List<RoomViewHolder> roomViewHolders = new ArrayList<>();
      
      @SuppressLint("HandlerLeak")
      public RoomAdapter(Context ctx, List<Device> devices, long roomId
      ) {
            this.devices = devices;
            this.ctx = ctx;
            long roomId1 = roomId;
      }
      
      @Override
      public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.device_layout, parent, false);
            
            float[] whiteMatrix = {1.0f, 1.0f, 1.0f, 0, 10, 1.0f, 1.0f, 1.0f, 0, 10, 1.0f, 1.0f,
                  1.0f, 0, 10, 0, 0, 0, 1, 0};
            whiteFilter = new ColorMatrixColorFilter(whiteMatrix);
            
            return new RoomViewHolder(view);
      }
      
      
      @Override
      public void onBindViewHolder(final RoomViewHolder holder,
            @SuppressLint("RecyclerView") final int position) {
            holder.deviceId = devices.get(holder.getAdapterPosition()).getDeviceId();
            holder.controllerId = devices.get(holder.getAdapterPosition()).getControllerId();
            roomViewHolders.add(holder);
            holder.textView.setText(devices.get(position).getName());
            try {
                  holder.imageView.setImageDrawable(
                        ctx.getResources().getDrawable(
                              ctx.getResources()
                                    .getIdentifier(devices.get(position).getIconName(), "drawable",
                                          ctx.getPackageName())
                        )
                  );
            } catch (Exception e) {
                  holder.imageView.setImageDrawable(
                        ctx.getResources().getDrawable(R.drawable.device_e)
                  );
            }
            
            if (devices.get(position).getStatus() == -1) {
                  holder.view.setEnabled(false);
                  holder.view.setBackgroundResource(R.color.colorAVG);
                  holder.wait.setVisibility(View.VISIBLE);
            } else if (devices.get(position).getStatus() > 0) {
                  holder.view.setEnabled(true);
                  holder.wait.setVisibility(View.GONE);
                  holder.imageView.setColorFilter(whiteFilter);
                  holder.view.setBackgroundResource(R.color.colorAccent);
                  holder.textView.setTextColor(ctx.getResources().getColor(R.color.colorTextDark));
            } else {
                  holder.view.setEnabled(true);
                  holder.wait.setVisibility(View.GONE);
                  holder.imageView.setColorFilter(null);
                  holder.view.setBackgroundResource(R.color.colorPrimary);
                  holder.textView
                        .setTextColor(ctx.getResources().getColor(R.color.colorTextPrimary));
            }
            new TcpClientAbstract() {
                  @Override
                  public void onResponse(TcpMessage tcpMessage) {
                        String deviceString = tcpMessage.getBody();
                        final Device device = new Gson()
                              .fromJson(deviceString, Device.class);
                        ((Activity) ctx).runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                    updateDevice(device);
                              }
                        });
                  }
            }.send(new TcpMessage() {{
                  setCommand(SV.TCP_COMMANDS.DEVICE.GET);
                  setBody(new Gson().toJson(devices.get(position), Device.class));
                  setBodyClass("Device");
            }});
            
            holder.view.setOnLongClickListener(new View.OnLongClickListener() {
                  @Override
                  public boolean onLongClick(View view) {
                        adapterActivityBridge.onDeviceLongClicked(position);
                        return false;
                  }
            });
            holder.view.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        adapterActivityBridge.onDeviceClicked(position);
                  }
            });
      }
      
      @Override
      public int getItemCount() {
            return devices.size();
      }
      
      public List<Device> getDevices() {
            return devices;
      }
      
      public void setDevices(List<Device> devices) {
            this.devices = devices;
      }
      
      public void clear() {
            this.devices = new ArrayList<>();
            notifyDataSetChanged();
      }
      
      public void updateDevice(Device device) {
            for (Device d : devices) {
                  if (d.getDeviceId() == device.getDeviceId()
                        && d.getControllerId() == device.getControllerId()) {
                        d.setStatus(device.getStatus());
                        notifyDataSetChanged();
                  }
            }
      }
      
      public void updateDeviceX(Device device) {
            for (RoomViewHolder roomViewHolder : roomViewHolders) {
                  if (roomViewHolder.deviceId == device.getDeviceId()
                        && roomViewHolder.controllerId == device.getControllerId()) {
                        roomViewHolder.view.setEnabled(true);
                        roomViewHolder.wait.setVisibility(View.GONE);
                        for (Device d : devices) {
                              if (d.getDeviceId() == device.getDeviceId()) {
                                    d.setStatus(device.getStatus());
                              }
                        }
                        notifyItemChanged(roomViewHolder.getAdapterPosition());
                  }
            }
      }
      
      private RoomViewHolder getDeviceViewHolder(Device device) {
            for (final RoomViewHolder roomViewHolder : roomViewHolders) {
                  if (roomViewHolder.deviceId == device.getDeviceId()
                        && roomViewHolder.controllerId == device.getControllerId()) {
                        return roomViewHolder;
                  }
            }
            return null;
      }
      
      public void waitingDevice(Device device) {
            
            for (final RoomViewHolder roomViewHolder : roomViewHolders) {
                  if (roomViewHolder.deviceId == device.getDeviceId()
                        && roomViewHolder.controllerId == device.getControllerId()) {
                        roomViewHolder.view.setEnabled(false);
                        roomViewHolder.view.setBackgroundResource(R.color.colorAVG);
                        roomViewHolder.wait.setVisibility(View.VISIBLE);
//                                    notifyItemChanged(roomViewHolder.getAdapterPosition());

//                        final Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                              @Override
//                              public void run() {
//                                    }
//                        }, 100);
//
                  }
                  
            }
      }
      
      // Container must implement this interface
      public void setAdapterActivityBridge(AdapterActivityBridge adapterActivityBridge) {
            this.adapterActivityBridge = adapterActivityBridge;
      }
      
      /**
       * Created by eagle on 3/28/2018.
       */
      
      public interface AdapterActivityBridge {
            
            void onDeviceClicked(int position);
            
            void onDeviceLongClicked(int position);
      }
}

class RoomViewHolder extends RecyclerView.ViewHolder {
      
      public long deviceId;
      public long controllerId;
      LinearLayout view;
      TextView textView;
      ImageView imageView;
      AVLoadingIndicatorView wait;
      
      RoomViewHolder(View itemView) {
            super(itemView);
            view = (LinearLayout) itemView;
            imageView = itemView.findViewById(R.id.imgIcon);
            textView = itemView.findViewById(R.id.txtName);
            wait = itemView.findViewById(R.id.wait);
            wait.setVisibility(View.GONE);
      }

//      @Override
//      public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
//            int position = viewHolder.getAdapterPosition();
//
//            if (direction == ItemTouchHelper.LEFT){
//                  Log.i("ItemSwipe", "LEFT");
//            }
//            else if (direction == ItemTouchHelper.RIGHT){
//                  Log.i("ItemSwipe", "RIGHT");
//            }
//      }

}