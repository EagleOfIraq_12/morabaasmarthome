package com.morabaa.team.morabaasmarthome.tcp;


import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.TcpMessage;
import com.morabaa.team.morabaasmarthome.utils.SV;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by eagle on 2/11/2018.
 */
public class TcpServer implements Runnable {

    public static TcpServer instant;
    private List<OnTcpMessageReceivedListener> onTcpMessageReceivedListeners
            = new ArrayList<>();

    public TcpServer() {
        setInstant(this);
    }

    public static TcpServer getInstant() {
        return instant;
    }

    private static void setInstant(TcpServer instant) {
        TcpServer.instant = instant;
    }

    @Override
    public void run() {
        System.out.println("Starting socket thread...");
        new Thread(() -> {
            try {
                ServerSocket serverSocket = new ServerSocket(21112);
                System.out
                        .println(
                                "ServerSocket created, waiting for incoming connections......");
                while (true) {
                    final Socket socket = serverSocket.accept();
                    new Thread(
                            () -> {
                                String message = null;
                                BufferedWriter out = null;
                                try {
                                    out = new BufferedWriter(

                                            new OutputStreamWriter(
                                                    socket.getOutputStream()));
                                    byte[] buffer = new byte[1400];
                                    int r = socket.getInputStream().read(buffer);
                                    message = new String(buffer, "UTF-8")
                                            .substring(0, r);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    assert message != null;
                                    message = message.substring(0, message.lastIndexOf(
                                            "}") + 1);
                                    System.out.println(
                                            "received from TCP: \t " + message);
                                    TcpMessage tcpMessage = new Gson()
                                            .fromJson(message,
                                                    TcpMessage.class);

                                    if (tcpMessage.getCommand().
                                            equals(SV.TCP_COMMANDS.DEVICE.GET) ||
                                            tcpMessage.getCommand()
                                                    .equals(SV.TCP_COMMANDS.DEVICE.SET)) {
                                        Device device = new Device();
                                        device.expand(tcpMessage.getBody());
                                        tcpMessage
                                                .setBody(
                                                        new Gson().toJson(
                                                                device, Device.class));
                                        tcpMessage.setBodyClass("Device");
                                    }
                                    for (OnTcpMessageReceivedListener listener :
                                            onTcpMessageReceivedListeners) {
                                        if (listener != null) {
                                            listener
                                                    .onTcpMessageReceived(tcpMessage);
                                        }
                                    }
                                    String outgoingMsg =
                                            "OK \t" + new Date().toString();
                                    out.write(outgoingMsg);
                                    out.flush();
                                    System.out.println("Message sent: " + outgoingMsg);
                                    socket.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                    ).start();
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }).start();
    }

    public void addOnTcpMessageReceivedListener(
            OnTcpMessageReceivedListener onTcpMessageReceivedListener) {
        onTcpMessageReceivedListeners.add(onTcpMessageReceivedListener);
        System.out.println(
                "onTcpMessageReceivedListenersCount : " + onTcpMessageReceivedListeners.size());
    }

    public void removeOnTcpMessageReceivedListener(
            OnTcpMessageReceivedListener onTcpMessageReceivedListener) {
        onTcpMessageReceivedListeners.remove(onTcpMessageReceivedListener);
        System.out.println(
                "onTcpMessageReceivedListenersCount : " + onTcpMessageReceivedListeners.size());
    }

    public interface OnTcpMessageReceivedListener {

        void onTcpMessageReceived(TcpMessage tcpMessage);
    }
}