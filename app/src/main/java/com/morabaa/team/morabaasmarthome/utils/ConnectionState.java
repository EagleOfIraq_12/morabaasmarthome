package com.morabaa.team.morabaasmarthome.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by eagle on 12/7/2017.
 */

public class ConnectionState {
      
      public static String getBssid(Context ctx) {
            WifiManager wifiManager = (WifiManager)
                  ctx.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            
            if (wInfo.getBSSID() != null) {
                  return wInfo.getBSSID().toUpperCase();
            }
            return "00:00:00:00:00:00";
      }
      
      public static String getLocalIpAddress() {
            try {
                  for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                        en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses();
                              enumIpAddr.hasMoreElements(); ) {
                              InetAddress inetAddress = enumIpAddr.nextElement();
                              if (!inetAddress.isLoopbackAddress()) {
                                    InetAddress address = inetAddress;
//                                    return intf.getInterfaceAddresses().get(1).getAddress()
//                                          .getHostName();
//                                    return address.getHostAddress();
                              }
                        }
                  }
            } catch (SocketException ex) {
                  Log.e("ip", ex.toString());
            }
            return "192.168.10.1";
      }
      
      /*
      	"18:A6:F7:92:5E:44"
      	"02:00:00:00:00:00"
      	"18:a6:f7:92:5e:44"
      */
      public static boolean isWifi(Context ctx) {
            ConnectivityManager cm = (ConnectivityManager) ctx
                  .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            return info != null && info.isConnected()
                  && info.getType() == ConnectivityManager.TYPE_WIFI;
      }
      
      public static void setIpAssignment(String assign, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                  Object ipConfiguration =
                        wifiConf.getClass().getMethod("getIpConfiguration")
                              .invoke(wifiConf);
                  setEnumField(ipConfiguration, assign, "ipAssignment");
            } else {
                  setEnumField(wifiConf, assign, "ipAssignment");
            }
      }
      
      public static void setIpAddress(InetAddress addr, int prefixLength,
            WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
            NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException {
            
            Object linkProperties = getField(wifiConf, "linkProperties");
            if (linkProperties == null) {
                  return;
            }
            Class laClass = Class.forName("android.net.LinkAddress");
            Constructor laConstructor = laClass
                  .getConstructor(new Class[]{InetAddress.class, int.class});
            Object linkAddress = laConstructor.newInstance(addr, prefixLength);
            
            ArrayList mLinkAddresses = (ArrayList) getDeclaredField(linkProperties,
                  "mLinkAddresses");
            mLinkAddresses.clear();
            mLinkAddresses.add(linkAddress);
      }
      
      public static void setGateway(InetAddress gateway, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
            ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException {
            Object linkProperties = getField(wifiConf, "linkProperties");
            if (linkProperties == null) {
                  return;
            }
            Class routeInfoClass = Class.forName("android.net.RouteInfo");
            Constructor routeInfoConstructor = routeInfoClass
                  .getConstructor(new Class[]{InetAddress.class});
            Object routeInfo = routeInfoConstructor.newInstance(gateway);
            
            ArrayList mRoutes = (ArrayList) getDeclaredField(linkProperties, "mRoutes");
            mRoutes.clear();
            mRoutes.add(routeInfo);
      }
      
      public static void setDNS(InetAddress dns, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
            Object linkProperties = getField(wifiConf, "linkProperties");
            if (linkProperties == null) {
                  return;
            }
            
            ArrayList<InetAddress> mDnses = (ArrayList<InetAddress>) getDeclaredField(
                  linkProperties, "mDnses");
            mDnses.clear(); //or add a new dns address , here I just want to replace DNS1
            mDnses.add(dns);
      }
      
      public static Object getField(Object obj, String name)
            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
            Field f = obj.getClass().getField(name);
            Object out = f.get(obj);
            return out;
      }
      
      public static Object getDeclaredField(Object obj, String name)
            throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException {
            Field f = obj.getClass().getDeclaredField(name);
            f.setAccessible(true);
            Object out = f.get(obj);
            return out;
      }
      
      private static void setEnumField(Object obj, String value, String name)
            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
            Field f = obj.getClass().getField(name);
            f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
      }
      
      public static WifiConfiguration getWifiConfiguration(Context ctx) {
            WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
            WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration conf : configuredNetworks) {
                  if (conf.networkId == connectionInfo.getNetworkId()) {
                        return conf;
                  }
            }
            return null;
      }
}
