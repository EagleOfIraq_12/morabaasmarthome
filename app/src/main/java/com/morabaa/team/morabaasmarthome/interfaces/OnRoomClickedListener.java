package com.morabaa.team.morabaasmarthome.interfaces;

import com.morabaa.team.morabaasmarthome.model.Room;

public interface OnRoomClickedListener {
      void onRoomClicked(Room room, boolean b);
}
