package com.morabaa.team.morabaasmarthome.fragments;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.adapters.LocationsAdapter;
import com.morabaa.team.morabaasmarthome.model.Room;

import java.util.ArrayList;
import java.util.List;

import static com.morabaa.team.morabaasmarthome.activities.MainActivity.db;

public class SelectLocationDialogFragment extends DialogFragment {

    int mNum;
    private TextView txtLocationName;
    private RecyclerView locationsRecyclerView;
    String itemName;
    long locationId;


    public static SelectLocationDialogFragment newInstance(int num, long locationId) {
        SelectLocationDialogFragment f = new SelectLocationDialogFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        args.putLong("locationId", locationId);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments().getInt("num");
        locationId = getArguments().getLong("locationId");
        itemName = db.roomDuo().getRoomNameByID(locationId);
        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        switch ((mNum - 1) % 6) {
            case 1:
                style = DialogFragment.STYLE_NO_TITLE;
                break;
            case 2:
                style = DialogFragment.STYLE_NO_FRAME;
                break;
            case 3:
                style = DialogFragment.STYLE_NO_INPUT;
                break;
            case 4:
                style = DialogFragment.STYLE_NORMAL;
                break;
            case 5:
                style = DialogFragment.STYLE_NORMAL;
                break;
            case 6:
                style = DialogFragment.STYLE_NO_TITLE;
                break;
            case 7:
                style = DialogFragment.STYLE_NO_FRAME;
                break;
            case 8:
                style = DialogFragment.STYLE_NORMAL;
                break;
        }
        switch ((mNum - 1) % 6) {
            case 4:
                theme = android.R.style.Theme_Holo;
                break;
            case 5:
                theme = android.R.style.Theme_Holo_Light_Dialog;
                break;
            case 6:
                theme = android.R.style.Theme_Holo_Light;
                break;
            case 7:
                theme = android.R.style.Theme_Holo_Light_Panel;
                break;
            case 8:
                theme = android.R.style.Theme_Holo_Light;
                break;
        }
        setStyle(style, theme);
    }

    View root;
    OnLocationSelectedListener onLocationSelectedListener;

    public void setOnLocationSelectedListener(OnLocationSelectedListener onLocationSelectedListener) {
        this.onLocationSelectedListener = onLocationSelectedListener;
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View root = inflater.inflate(R.layout.select_location_dialog_layout, container, false);
        this.root = root;

        locationsRecyclerView = root.findViewById(R.id.locationsRecyclerView);
        txtLocationName = root.findViewById(R.id.txtLocationName);
        List<Room> rooms = new ArrayList<>();
        rooms = db.roomDuo().ROOMS();
        locationsRecyclerView.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false)
        );
        LocationsAdapter locationsAdapter = new LocationsAdapter(getContext(), rooms);
        locationsAdapter.setOnLocationSelectedListener(new LocationsAdapter.OnLocationSelectedListener() {
            @Override
            public void onLocationSelected(long locationId) {
                onLocationSelectedListener.onLocationSelected(
                        locationId
                );
            }
        });
        locationsRecyclerView.setAdapter(locationsAdapter);
        txtLocationName.setText(itemName);


        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        return root;
    }


    public interface OnLocationSelectedListener {
        void onLocationSelected(long locationId);
    }
}