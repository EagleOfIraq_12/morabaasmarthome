package com.morabaa.team.morabaasmarthome.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.fragments.SelectLocationDialogFragment;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.Relation;
import com.morabaa.team.morabaasmarthome.model.Room;

import java.util.List;

import static com.morabaa.team.morabaasmarthome.activities.MainActivity.db;

public class DeviceSettingsActivity extends AppCompatActivity {

    private long deviceId;
    private Device device;
    private ImageView imgIcon;
    private RadioButton radJustMe;
    private RadioButton radAll;
    private EditText txtDeviceName;
    private TextView txtDeviceLocation;
    private LinearLayout layoutDeviceLocation;
    private EditText txtDeviceType;
    private Button btnDeviceTypeMultiValue;
    private Button btnDeviceTypeSingle;
    private EditText txtDeviceAmperes;
    private Button btnSave;
    private Button btnAddRelation;
    long locationId;
    private Spinner spinnerRelationType;
    private EditText txtOtherDeviceId;

    private int relationType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_settings);
        deviceId = getIntent().getLongExtra("deviceId", 0);
        device = db.deviceDuo().getDeviceById(deviceId);
        setTitle("الاعدادات");
        txtDeviceName = findViewById(R.id.txtDeviceName);
        radJustMe = findViewById(R.id.radJustMe);
        radAll = findViewById(R.id.radAll);
        radJustMe.setChecked(true);
        imgIcon = findViewById(R.id.imgIcon);
        txtDeviceLocation = findViewById(R.id.txtDeviceLocation);
        layoutDeviceLocation = findViewById(R.id.layoutDeviceLocation);
        txtDeviceType = findViewById(R.id.txtDeviceType);
        btnDeviceTypeMultiValue = findViewById(R.id.btnDeviceTypeMultiValue);
        btnDeviceTypeSingle = findViewById(R.id.btnDeviceTypeSingle);
        txtDeviceAmperes = findViewById(R.id.txtDeviceAmperes);
        btnSave = findViewById(R.id.btnSave);
        btnAddRelation = findViewById(R.id.btnAddRelation);

        txtOtherDeviceId = findViewById(R.id.txtOtherDeviceId);
        spinnerRelationType = findViewById(R.id.spinnerRelationType);

        List<String> types = Relation.TYPE.getNames();
        ArrayAdapter<String> typesAdapter = new ArrayAdapter<>(
                DeviceSettingsActivity.this,
                android.R.layout.simple_spinner_item, types);
        typesAdapter
                .setDropDownViewResource(
                        android.R.layout.simple_spinner_dropdown_item);

        spinnerRelationType.setAdapter(typesAdapter);

        btnDeviceTypeMultiValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                device.setType("multiValue");
                checkDeviceType(device.getType());
            }
        });
        btnDeviceTypeSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                device.setType("single");
                checkDeviceType(device.getType());
            }
        });

        spinnerRelationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String type = spinnerRelationType.getSelectedItem().toString();
                try {
                    relationType = Relation.TYPE.getIdByName(type);
                } catch (Exception ignored) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnAddRelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Relation relation = new Relation();
                relation.setDeviceId(deviceId);
                relation.setRelationType(relationType);
                relation.setToConnectDeviceId(
                        Long.parseLong(txtOtherDeviceId.getText().toString())
                );
                db.relationDuo().insert(relation);
                Toast.makeText(DeviceSettingsActivity.this, "" + db.relationDuo().count(), Toast.LENGTH_SHORT).show();
            }
        });

        if (device != null) {
            setTitle("اعدادات " + device.getName());
            txtDeviceName.setText(device.getName());
            Room room = db.roomDuo().getRoomById(device.getRoomId());
            txtDeviceLocation.setText(
                    room.getName()
            );
            imgIcon.setImageDrawable(
                    DeviceSettingsActivity.this.getResources().getDrawable(
                            DeviceSettingsActivity.this.getResources()
                                    .getIdentifier(room.getIconName(),
                                            "drawable",
                                            DeviceSettingsActivity.this.getPackageName())
                    )
            );

            locationId = db.roomDuo().getRoomIDByName(txtDeviceLocation.getText().toString());
            layoutDeviceLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final SelectLocationDialogFragment locationsDialog = SelectLocationDialogFragment.newInstance(2, locationId);
                    locationsDialog.setCancelable(true);
                    locationsDialog.show(getFragmentManager(), "Diag");

                    locationsDialog.setOnLocationSelectedListener(new SelectLocationDialogFragment.OnLocationSelectedListener() {
                        @Override
                        public void onLocationSelected(long lId) {
                            locationId = lId;
                            Room room = db.roomDuo().getRoomById(lId);

                            txtDeviceLocation.setText(
                                    room.getName()
                            );
                            imgIcon.setImageDrawable(
                                    DeviceSettingsActivity.this.getResources().getDrawable(
                                            DeviceSettingsActivity.this.getResources()
                                                    .getIdentifier(room.getIconName(),
                                                            "drawable",
                                                            DeviceSettingsActivity.this.getPackageName())
                                    )
                            );
                            locationsDialog.dismiss();
                        }
                    });
                }
            });
            checkDeviceType(device.getType());

            txtDeviceType.setText(device.getType());
//            txtDeviceAmperes.setText((int) device.getAmperes());
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    device.setName(txtDeviceName.getText().toString());
                    device.setRoomId(locationId);

//                    device.setAmperes(Float.parseFloat(txtDeviceAmperes.getText().toString()));
                    device.setType(txtDeviceType.getText().toString());
                    db.deviceDuo().Update(device);
                    onBackPressed();
                }
            });
        }
    }

    void checkDeviceType(String type) {
        if (type.equals("single")) {
            btnDeviceTypeSingle.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            btnDeviceTypeMultiValue.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            btnDeviceTypeSingle.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            btnDeviceTypeMultiValue.setTextColor(getResources().getColor(R.color.colorAccent));
        } else {
            btnDeviceTypeSingle.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            btnDeviceTypeMultiValue.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            btnDeviceTypeSingle.setTextColor(getResources().getColor(R.color.colorAccent));
            btnDeviceTypeMultiValue.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }
}
