package com.morabaa.team.morabaasmarthome.tcp;


import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.TcpMessage;
import com.morabaa.team.morabaasmarthome.utils.SV;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by eagle on 2/11/2018.
 */
public class TcpServerX {

    public static TcpServerX instant;
    Socket socket = null;
    private List<OnTcpMessageReceivedListener> onTcpMessageReceivedListeners = new ArrayList<>();
//      private OnTcpMessageReceivedListener onTcpMessageReceivedListener;

    public TcpServerX() {
        setInstant(this);
    }

    public static TcpServerX getInstant() {
        return instant;
    }

    private static void setInstant(TcpServerX instant) {
        TcpServerX.instant = instant;
    }

//      public void setOnTcpMessageReceivedListener(
//            OnTcpMessageReceivedListener onTcpMessageReceivedListener) {
//            this.onTcpMessageReceivedListener = onTcpMessageReceivedListener;
//      }

    public void run() {
        new Thread(() -> {
            String incomingMsg = "";
            try {
                if (socket != null && !socket.isClosed()) {
                    socket.close();
                }
                System.out.println("Starting socket thread...");
                ServerSocket serverSocket = new ServerSocket(21112);
                System.out
                        .println(
                                "ServerSocket created, waiting for incoming connections......");
                while (true) {
                    socket = serverSocket.accept();
                    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                            socket.getOutputStream()));
//                                    SV.SERVER_IP = socket.getInetAddress().getHostAddress();
                    byte[] buffer = new byte[1400];
                    int r = socket.getInputStream().read(buffer, 0, buffer.length);
                    incomingMsg = new String(buffer);
                    System.out.println("Message received from Tcp: " + incomingMsg);
                    incomingMsg = incomingMsg
                            .substring(0, incomingMsg.lastIndexOf("}") + 1);

                    if (incomingMsg.contains(SV.TCP_COMMANDS.DEVICE.GET) ||
                            incomingMsg.contains(SV.TCP_COMMANDS.DEVICE.SET)) {
                        incomingMsg = incomingMsg.substring(0,
                                incomingMsg.lastIndexOf("}") + 1);
                        TcpMessage tcpMessage = new Gson()
                                .fromJson(incomingMsg, TcpMessage.class);
                        // body= c,d,s;
                        String[] body = tcpMessage.getBody().split(",");
                        long c = Long.parseLong(body[0]);
                        long d = Long.parseLong(body[1]);
                        int s = Integer.parseInt(body[2]);

                        Device device = new Device();
//                                device.setId(
//                                        Long.parseLong(
//                                               c +String.format("%3d0", d, Locale.US)
//                                        )
//                                );
                        device.setDeviceId(d);
                        device.setControllerId(c);
                        device.setStatus(s);
                        tcpMessage
                                .setBody(new Gson().toJson(device, Device.class));
                        tcpMessage.setBodyClass("Device");
//                                          onTcpMessageReceivedListener
//                                                .onTcpMessageReceived(tcpMessage);
                        for (OnTcpMessageReceivedListener onTcpMessageReceivedListener : onTcpMessageReceivedListeners) {
                            if (onTcpMessageReceivedListener != null) {
                                onTcpMessageReceivedListener
                                        .onTcpMessageReceived(tcpMessage);
                            }
                        }
                        incomingMsg = "OK";
                        out.write(incomingMsg);
                        out.flush();
                        System.out
                                .println("Sent \tto TCP: \t" + incomingMsg);
                        socket.close();
                    } else {
                        int i = 0;
                        while (socket.isConnected() && i == 0) {
                            TcpMessage tcpMessage = new Gson()
                                    .fromJson(incomingMsg, TcpMessage.class);
                            for (OnTcpMessageReceivedListener onTcpMessageReceivedListener : onTcpMessageReceivedListeners) {
                                if (onTcpMessageReceivedListener != null) {
                                    onTcpMessageReceivedListener
                                            .onTcpMessageReceived(tcpMessage);
                                }
                            }
                            // checkIp a message
                            String outgoingMsg =
                                    "OK \t" + new Date().toString();
                            out.write(outgoingMsg);
                            out.flush();
                            System.out.println("Message sent: " + outgoingMsg);
                            socket.close();
                            i = 1;
                        }
                    }

                }

            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                try {
                    socket.close();
                } catch (IOException e1) {
                }
            }
        }).start();
    }

    public void addOnTcpMessageReceivedListener(
            OnTcpMessageReceivedListener onTcpMessageReceivedListener) {
        onTcpMessageReceivedListeners.add(onTcpMessageReceivedListener);
        System.out.println(
                "onTcpMessageReceivedListenersCount : " + onTcpMessageReceivedListeners.size());
    }

    public void removeOnTcpMessageReceivedListener(
            OnTcpMessageReceivedListener onTcpMessageReceivedListener) {
        onTcpMessageReceivedListeners.remove(onTcpMessageReceivedListener);
        System.out.println(
                "onTcpMessageReceivedListenersCount : " + onTcpMessageReceivedListeners.size());
    }

    public interface OnTcpMessageReceivedListener {

        void onTcpMessageReceived(TcpMessage tcpMessage);
    }
}