package com.morabaa.team.morabaasmarthome.utils;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by eagle on 1/16/2018.
 */

public class SV {
      
      public static String DEVICE_ID = "";
      public static String SERVER_IP = "192.168.10.121";
      
      public static class TCP_COMMANDS {
            
            public static class DEVICE {
                  
                  public static final String GET = "get";
                  public static final String SET = "set";
                  public static final String UPDATE = "updateDevice";
                  public static final String ADD = "addDevice";
                  public static final String GET_ALL = "getDevices";
                  public static final String TURN_ALL_ON = "turnAllOn";
                  public static final String TURN_ALL_OFF = "turnAllOff";
                  
            }
            
            public static class Room {
                  
                  public static final String GET_ROOMS = "getRooms";
                  public static final String TURN_ROOM_OFF = "turnRoomOff";
                  public static final String TURN_ROOM_ON = "turnRoomOn";
                  public static final String GET_ROOM_DEVICES = "getRoomDevices";
            }
            
            public static class RELATION {
                  
                  public static final String ADD = "addRelation";
            }
            
            public static class OTHER {
                  public static final String REFRESH = "refresh";
                  public static final String CHECK_SERVER = "areYouServer";
                  public static final String DEBUG = "debug";
                  public static final String RESET = "reset";
                  public static final String REBOOT = "reboot";
            }
            
      }
      
      public static class ScreenDimensions {
            
            public static int getHeight(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.heightPixels;
            }
            
            public static int getWidth(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.widthPixels;
            }
      }
}
