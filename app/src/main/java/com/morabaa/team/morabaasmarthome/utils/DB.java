package com.morabaa.team.morabaasmarthome.utils;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.Device.DeviceDuo;
import com.morabaa.team.morabaasmarthome.model.Floor;
import com.morabaa.team.morabaasmarthome.model.Floor.FloorDuo;
import com.morabaa.team.morabaasmarthome.model.Relation;
import com.morabaa.team.morabaasmarthome.model.Room;
import com.morabaa.team.morabaasmarthome.model.Room.RoomDuo;

/**
 * Created by eagle on 2/6/2018.
 */
@Database(entities = {
        Device.class,
        Room.class,
        Floor.class,
        Relation.class
}, version = 9)
public abstract class DB extends RoomDatabase {

    public abstract DeviceDuo deviceDuo();

    public abstract RoomDuo roomDuo();

    public abstract FloorDuo floorDuo();

    public abstract Relation.RelationDuo relationDuo();

}
