package com.morabaa.team.morabaasmarthome.tcp;


import static com.morabaa.team.morabaasmarthome.utils.SV.SERVER_IP;

import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.TcpMessage;
import com.morabaa.team.morabaasmarthome.utils.SV;
import java.io.DataOutputStream;
import java.net.Socket;

/**
 * Created by eagle on 2/11/2018.
 */

public class TcpClient {

    public TcpClient() {
    }

    public static void send(final TcpMessage tcpMessage) {
        new Thread(new Runnable() {
            String receivedMessage = "";

            @Override
            public void run() {
                if (tcpMessage.getCommand().equals(SV.TCP_COMMANDS.DEVICE.GET) ||
                        tcpMessage.getCommand().equals(SV.TCP_COMMANDS.DEVICE.SET)) {
                    Device device = new Gson().fromJson(tcpMessage.getBody(), Device.class);
                    String bdy = device.getControllerId() + "," +
                            device.getDeviceId() + "," +
                            device.getStatus();
                    tcpMessage.setBody(bdy);
                }
                String message = new Gson().toJson(tcpMessage, TcpMessage.class);
                Socket clientSocket = null;
                try {
                    clientSocket = new Socket(SERVER_IP, 21111);
                    DataOutputStream outToServer = new DataOutputStream(
                            clientSocket.getOutputStream());
                    outToServer.write(message.getBytes());
                    System.out.println(
                            "sent To TCP SERVER: " + SERVER_IP + " "
                                    + message);
                    byte[] buffer = new byte[1400];
                    int r = clientSocket.getInputStream().read(buffer);
                    receivedMessage = new String(buffer).substring(0, r);
                    if (receivedMessage.length() > 0) {
                        System.out.println("FROM SERVER: " + receivedMessage);
                    }
                    clientSocket.close();
                } catch (Exception e) {
                }
            }
        }).start();
    }
}