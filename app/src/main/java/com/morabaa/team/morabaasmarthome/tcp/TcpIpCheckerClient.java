package com.morabaa.team.morabaasmarthome.tcp;


import com.morabaa.team.morabaasmarthome.utils.SV;
import java.io.DataOutputStream;
import java.net.Socket;

/**
 * Created by eagle on 2/11/2018.
 */

public abstract class TcpIpCheckerClient {
      
      private String TEMP_SERVER_IP;
      
      public TcpIpCheckerClient(String TEMP_SERVER_IP) {
            this.TEMP_SERVER_IP ="192.168.10.41"; //TEMP_SERVER_IP;
            System.out.println(
                  "CHECK TCP SERVER  :" + TEMP_SERVER_IP);
            checkIp();
      }
      
      public void checkIp() {
            String receivedMessage = "";
            String message = SV.TCP_COMMANDS.OTHER.CHECK_SERVER;
            Socket clientSocket = null;
            try {
                  clientSocket = new Socket(TEMP_SERVER_IP, 21111);
                  DataOutputStream outToServer = new DataOutputStream(
                        clientSocket.getOutputStream());
                  outToServer.write(message.getBytes());
                  System.out.println(
                        "sent To TCP SERVER: " + TEMP_SERVER_IP + " "
                              + message);
                  byte[] buffer = new byte[1400];
                  int r = clientSocket.getInputStream().read(buffer);
                  receivedMessage = new String(buffer).substring(0, r);
                  if (receivedMessage.equals("yes")) {
                        exist(true);
                        System.out.println("FROM SERVER: " + receivedMessage);
                  } else {
                        exist(false);
                  }
                  clientSocket.close();
            } catch (Exception ignored) {
                  exist(false);
            }
      }
      
      public abstract void exist(boolean there);
      
}