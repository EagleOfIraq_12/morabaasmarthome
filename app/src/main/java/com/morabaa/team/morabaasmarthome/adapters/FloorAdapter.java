package com.morabaa.team.morabaasmarthome.adapters;

import static android.support.v4.app.ActivityOptionsCompat.makeSceneTransitionAnimation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.activities.RoomActivity;
import com.morabaa.team.morabaasmarthome.interfaces.OnRoomClickedListener;
import com.morabaa.team.morabaasmarthome.model.Room;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eagle on 11/2/2017.
 */

public class FloorAdapter extends RecyclerView.Adapter<FloorViewHolder> {
      
      List<Room> rooms;
      Context ctx;
      String floorId;
      OnRoomClickedListener roomClickedListener;
      private Vibrator vibrator;
      
      public FloorAdapter(Context ctx, List<Room> rooms, String floorId) {
            this.rooms = rooms;
            this.ctx = ctx;
            this.floorId = floorId;
      }
      
      @Override
      public FloorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.room_layout, parent, false);
            
            return new FloorViewHolder(view);
      }
      
      @Override
      public void onBindViewHolder(final FloorViewHolder holder, final int position) {
            vibrator = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
            roomClickedListener = (OnRoomClickedListener) ctx;
            holder.textView.setText(rooms.get(holder.getAdapterPosition()).getName());
            holder.imageView.setImageDrawable(
                  ctx.getResources().getDrawable(
                        ctx.getResources()
                              .getIdentifier(rooms.get(holder.getAdapterPosition()).getIconName(),
                                    "drawable",
                                    ctx.getPackageName())
                  )
            );
            holder.view.setOnClickListener(
                  new View.OnClickListener() {
                        @SuppressLint("StaticFieldLeak")
                        @Override
                        public void onClick(View view) {
                              
                              String roomName = rooms.get(position).getName();
                              long roomId = rooms.get(position).getId();
                              
                              ImageView imageView = holder.imageView;
                              TextView textView = holder.textView;
                              List<Pair<View, String>> pairs = new ArrayList<>();
                              
                              pairs.add(new Pair<View, String>(textView,
                                    RoomActivity.VIEW_NAME_HEADER_TITLE));
                              pairs.add(new Pair<View, String>(imageView,
                                    RoomActivity.VIEW_NAME_HEADER_IMAGE));
                              
                              ActivityOptionsCompat activityOptions = makeSceneTransitionAnimation(
                                    (Activity) ctx, pairs.get(0), pairs.get(1)
                              );
                              
                              Intent intent = new Intent(ctx, RoomActivity.class);
                              intent.putExtra("roomName", roomName);
                              intent.putExtra("roomId", roomId);
                              ContextCompat.startActivity(ctx, intent, activityOptions.toBundle());
//                              for (Device device : db.deviceDuo().getDevicesByRoomId(roomId)) {
//                                    TcpMessage tcpMessage = new TcpMessage();
//                                    tcpMessage.setCommand(TCP_COMMANDS.GET);
//                                    device.setStatus(1);
//                                    tcpMessage.setDevice(device);
//                                    TcpClient.checkIp(ctx,
//                                          new Gson().toJson(tcpMessage, TcpMessage.class));
//                              }
                        }
                  });
            
            holder.layoutTurnAllOff.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        vibrator.vibrate(70);
                        roomClickedListener.onRoomClicked(rooms.get(position),false);
                  }
            });
            holder.layoutTurnAllOn.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        vibrator.vibrate(70);
                        roomClickedListener.onRoomClicked(rooms.get(position),true);
                  }
            });
            
            
      }
      
      @Override
      public int getItemCount() {
            return rooms.size();
      }
      
}

class FloorViewHolder extends RecyclerView.ViewHolder {
      
      LinearLayout view;
      TextView textView;
      ImageView imageView;
      LinearLayout layoutTurnAllOff;
      LinearLayout layoutTurnAllOn;
      
      public FloorViewHolder(View itemView) {
            super(itemView);
            view = (LinearLayout) itemView;
            imageView = itemView.findViewById(R.id.imgIcon);
            textView = itemView.findViewById(R.id.txtName);
            
            layoutTurnAllOn = itemView.findViewById(R.id.layoutTurnAllOn);
            layoutTurnAllOff = itemView.findViewById(R.id.layoutTurnAllOff);
      }
}