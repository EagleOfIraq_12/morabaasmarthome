package com.morabaa.team.morabaasmarthome.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.morabaa.team.morabaasmarthome.NSDDiscover;
import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.utils.SV;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class WelcomeActivity extends AppCompatActivity {

    private NSDDiscover mNSDDiscover;

//    RippleBackground rippleBackground;

    Button btnSetDeviceId;
    Button btnQrDeviceId;
    EditText txtDeviceId;
    LinearLayout layoutDeviceId;


    Button btnSetDeviceIp;
    Button btnQrDeviceIp;
    Button btnSearchDeviceIp;
    EditText txtDeviceIp;
    LinearLayout layoutDeviceIp;
    boolean f = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mNSDDiscover = new NSDDiscover(this, mDiscoveryListener);

//        mNSDListener.registerDevice();

        mNSDDiscover.discoverServices();

//        goToMain();
//            rippleBackground = findViewById(R.id.content);
//            ImageView imageView = findViewById(R.id.centerImage);
//            imageView.setOnClickListener(new OnClickListener() {
//                  @Override
//                  public void onClick(View view) {
//                        searchIp();
//                  }
//            });
//            rippleBackground.startRippleAnimation();
//            checkId();

    }

    private NSDDiscover.DiscoveryListener mDiscoveryListener = (host, port) -> {
        //This callback is on a worker thread...
//        runOnUiThread(() -> {
        SV.SERVER_IP = host;
        goToMain();
//                }
//        );
    };

//      private void checkId() {
//            btnQrDeviceId = findViewById(R.id.btnQrDeviceId);
//            txtDeviceId = findViewById(R.id.txtDeviceId);
//            btnSetDeviceId = findViewById(R.id.btnSetDeviceId);
//            layoutDeviceId = findViewById(R.id.layoutDeviceId);
//            LocalData.add(this, "DEVICE_ID", "");
//            LocalData.add(this, "DEVICE_ID", "0c8c6982c3a218f5");
//            if (LocalData.get(WelcomeActivity.this, "DEVICE_ID").length() > 0) {
//                  checkIp();
//            } else {
//                  layoutDeviceId.setVisibility(View.VISIBLE);
//            }
//
//            btnQrDeviceId.setOnClickListener(new OnClickListener() {
//                  @Override
//                  public void onClick(View view) {
//                        IntentIntegrator integrator = new IntentIntegrator(WelcomeActivity.this);
//                        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
//                        integrator.setPrompt("Scan Code");
//                        integrator.setCameraId(0);
//                        integrator.setBeepEnabled(true);
//                        integrator.setBarcodeImageEnabled(false);
//                        integrator.initiateScan();
//                  }
//            });
//            txtDeviceId.setText("0c8c6982c3a218f5");
//            btnSetDeviceId.setOnClickListener(new OnClickListener() {
//                  @Override
//                  public void onClick(View view) {
//                        String key = txtDeviceId.getText().toString();
//                        LocalData.add(WelcomeActivity.this, "DEVICE_ID", key);
//                        checkIp();
//                  }
//            });
//      }

//      @Override
//      protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//            super.onActivityResult(requestCode, resultCode, data);
//            IntentResult intentResult = IntentIntegrator
//                  .parseActivityResult(requestCode, resultCode, data);
//            if (intentResult != null) {
//                  if (intentResult.getContents() == null) {
//                        Log.d("MainActivity", "Cancelled");
//                        Toast.makeText(this, "Cancelled",
//                              Toast.LENGTH_LONG).show();
//                  } else {
//                        Log.d("MainActivity", "Scanned");
//                        String contents = intentResult.getContents();
//                        TcpMessage tcpMessage = new Gson().fromJson(contents, TcpMessage.class);
//                        String id = "";
//                        String ip = "";
//                        if (tcpMessage.getCommand().equals("id")) {
//                              Device device = new Gson()
//                                    .fromJson(tcpMessage.getBody(), Device.class);
//                              id = device.getName();
//                              ip = device.getType();
//                        }
//                        LocalData.add(WelcomeActivity.this, "DEVICE_ID", id);
//                        LocalData.add(WelcomeActivity.this, "SERVER_IP", ip);
//                        goToMain();
//                  }
//            }
//      }
//
//      void checkIp() {
//
//            layoutDeviceId.setVisibility(View.GONE);
//
//            btnQrDeviceIp = findViewById(R.id.btnQrDeviceIp);
//            txtDeviceIp = findViewById(R.id.txtDeviceIp);
//            btnSetDeviceIp = findViewById(R.id.btnSetDeviceIp);
//            layoutDeviceIp = findViewById(R.id.layoutDeviceIp);
//            btnSearchDeviceIp = findViewById(R.id.btnSearchDeviceIp);
////            SERVER_IP = "192.168.10.121";
//            goToMain();
////            LocalData.add(WelcomeActivity.this,
////                  "SERVER_IP", SERVER_IP);
////            if (LocalData.get(WelcomeActivity.this, "SERVER_IP").length() > 0) {
////                  new Thread(new Runnable() {
////                        @Override
////                        public void run() {
////                              new TcpIpCheckerClient(LocalData.get(WelcomeActivity.this,
////                                    "SERVER_IP")) {
////                                    @Override
////                                    public void exist(boolean there) {
////                                          if (there) {
////                                                SERVER_IP = LocalData.get(WelcomeActivity.this,
////                                                      "SERVER_IP");
////                                                goToMain();
////                                          }
////                                    }
////                              };
////                        }
////                  }).start();
////            } else {
////                  searchIp();
////            }
////            String myIp = ConnectionState.getLocalIpAddress();
////            String TEMP_SERVER_IP =
////                  myIp.substring(0, myIp.lastIndexOf(".") + 1) + ".";
////            txtDeviceIp.setText(TEMP_SERVER_IP);
////
////            btnQrDeviceIp.setOnClickListener(new OnClickListener() {
////                  @Override
////                  public void onClick(View view) {
////                        IntentIntegrator integrator = new IntentIntegrator(WelcomeActivity.this);
////                        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
////                        integrator.setPrompt("Scan Code");
////                        integrator.setCameraId(0);
////                        integrator.setBeepEnabled(true);
////                        integrator.setBarcodeImageEnabled(false);
////                        integrator.initiateScan();
////                  }
////            });
////            txtDeviceIp.setText("192.168.10.");
////            btnSetDeviceIp.setOnClickListener(new OnClickListener() {
////                  @Override
////                  public void onClick(View view) {
////                        final String key = txtDeviceIp.getText().toString();
////                        new Thread(new Runnable() {
////                              @Override
////                              public void run() {
////                                    new TcpIpCheckerClient(key) {
////                                          @Override
////                                          public void exist(boolean there) {
////                                                if (there) {
////                                                      LocalData.add(WelcomeActivity.this,
////                                                            "SERVER_IP", key);
////                                                      SERVER_IP = key;
////                                                      goToMain();
////                                                } else {
////                                                      runOnUiThread(new Runnable() {
////                                                            @Override
////                                                            public void run() {
////                                                                  Toast.makeText(
////                                                                        WelcomeActivity.this,
////                                                                        " الجهاز " + key +
////                                                                              "غير مرتبط بالشبكه ",
////                                                                        Toast.LENGTH_SHORT).show();
////                                                            }
////                                                      });
////                                                }
////                                          }
////                                    };
////                              }
////                        }).start();
////                  }
////            });
////            btnSetDeviceIp.setOnLongClickListener(new View.OnLongClickListener() {
////                  @Override
////                  public boolean onLongClick(View v) {
////                        goToMain();
////                        return false;
////                  }
////            });
////            btnSearchDeviceIp.setOnClickListener(new OnClickListener() {
////                  @Override
////                  public void onClick(View v) {
////                        searchIp();
////                  }
////            });
//      }
//
//      public void searchIp() {
//            String myIp = ConnectionState.getLocalIpAddress();
//            int i = 0;
//            while (i++ < 256 && SERVER_IP.equals("")) {
//                  final String TEMP_SERVER_IP = myIp.substring(0, myIp.lastIndexOf(".") + 1) + i;
//                  final int finalI = i;
//                  new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                              new TcpIpCheckerClient(TEMP_SERVER_IP) {
//                                    @Override
//                                    public void exist(boolean there) {
//                                          f = there;
//                                          if (there) {
//                                                LocalData.add(WelcomeActivity.this,
//                                                      "SERVER_IP",
//                                                      TEMP_SERVER_IP);
//                                                SERVER_IP = TEMP_SERVER_IP;
//                                                goToMain();
//                                          } else if (finalI == 255) {
//
//                                                runOnUiThread(new Runnable() {
//                                                      @Override
//                                                      public void run() {
//                                                            new Handler(Looper.getMainLooper())
//                                                                  .postDelayed(new Runnable() {
//                                                                        @Override
//                                                                        public void run() {
//                                                                              if (!f) {
//                                                                                    rippleBackground
//                                                                                          .setVisibility(
//                                                                                                View.GONE);
//                                                                                    layoutDeviceIp
//                                                                                          .setVisibility(
//                                                                                                View.VISIBLE);
//
//                                                                              }
//                                                                        }
//                                                                  }, 4000);
//                                                      }
//                                                });
//                                          }
//                                    }
//                              };
//                        }
//                  }).start();
//            }
//      }

    void goToMain() {
        Intent resultIntent = new Intent(WelcomeActivity.this,
                MainActivity.class);
        startActivity(resultIntent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mNSDDiscover.shutdown();
    }

}
