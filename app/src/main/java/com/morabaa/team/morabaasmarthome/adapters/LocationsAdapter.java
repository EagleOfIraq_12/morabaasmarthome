package com.morabaa.team.morabaasmarthome.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.model.Room;

import java.util.List;

import static android.support.v4.app.ActivityOptionsCompat.makeSceneTransitionAnimation;

/**
 * Created by eagle on 11/2/2017.
 */

public class LocationsAdapter extends RecyclerView.Adapter<LocationsViewHolder> {

    List<Room> locations;
    Context ctx;
    OnLocationSelectedListener onLocationSelectedListener;

    public void setOnLocationSelectedListener(OnLocationSelectedListener onLocationSelectedListener) {
        this.onLocationSelectedListener = onLocationSelectedListener;
    }

    public LocationsAdapter(Context ctx, List<Room> locations) {
        this.locations = locations;
        this.ctx = ctx;
    }

    @Override
    public LocationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.room_layout, parent, false);

        return new LocationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LocationsViewHolder holder, final int position) {
        holder.textView.setText(locations.get(holder.getAdapterPosition()).getName());
        holder.imageView.setImageDrawable(
                ctx.getResources().getDrawable(
                        ctx.getResources()
                                .getIdentifier(locations.get(holder.getAdapterPosition()).getIconName(),
                                        "drawable",
                                        ctx.getPackageName())
                )
        );
        holder.view.setOnClickListener(
                new OnClickListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onClick(View view) {
                        long locationId = locations.get(position).getId();
                        onLocationSelectedListener.onLocationSelected(locationId);
                    }
                });

    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    public interface OnLocationSelectedListener {
        void onLocationSelected(long locationId);
    }

}

class LocationsViewHolder extends RecyclerView.ViewHolder {

    LinearLayout view;
    TextView textView;
    ImageView imageView;
    LinearLayout layoutTurnAllOff;
    LinearLayout layoutTurnAllOn;

    public LocationsViewHolder(View itemView) {
        super(itemView);
        view = (LinearLayout) itemView;
        imageView = itemView.findViewById(R.id.imgIcon);
        textView = itemView.findViewById(R.id.txtName);
        layoutTurnAllOn = itemView.findViewById(R.id.layoutTurnAllOn);
        layoutTurnAllOff = itemView.findViewById(R.id.layoutTurnAllOff);
        layoutTurnAllOff.setVisibility(View.GONE);
        layoutTurnAllOn.setVisibility(View.GONE);
    }
}