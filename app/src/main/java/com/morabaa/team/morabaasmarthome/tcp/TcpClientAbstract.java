package com.morabaa.team.morabaasmarthome.tcp;


import static com.morabaa.team.morabaasmarthome.utils.SV.SERVER_IP;

import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.TcpMessage;
import com.morabaa.team.morabaasmarthome.utils.SV;
import java.io.DataOutputStream;
import java.net.Socket;

/**
 * Created by eagle on 2/11/2018.
 */

public abstract class TcpClientAbstract {
      
      public TcpClientAbstract() {
      }
      
      public void send(final TcpMessage tcpMessage) {
            if (true)return;
            
            new Thread(new Runnable() {
                  String receivedMessage = "";
                  
                  @Override
                  public void run() {
                        if (tcpMessage.getCommand().contains(SV.TCP_COMMANDS.DEVICE.GET) ||
                              tcpMessage.getCommand().contains(SV.TCP_COMMANDS.DEVICE.SET)) {
                              Device device = new Gson()
                                    .fromJson(tcpMessage.getBody(), Device.class);
                              String bdy = device.getControllerId() + "," +
                                    device.getDeviceId() + "," +
                                    device.getStatus();
                              tcpMessage.setBody(bdy);
                        }
                        String message = new Gson().toJson(tcpMessage, TcpMessage.class);
                        System.out.println("SERVER_IP: " + SERVER_IP);
                        Socket clientSocket = null;
                        try {
                              clientSocket = new Socket(SERVER_IP, 21111);
                              DataOutputStream outToServer = new DataOutputStream(
                                    clientSocket.getOutputStream());
                              outToServer.write(message.getBytes());
                              System.out.println(
                                    "sent To TCP SERVER: " + SERVER_IP + " "
                                          + message);
                              byte[] buffer = new byte[1400];
                              int r = clientSocket.getInputStream().read(buffer);
                              receivedMessage = new String(buffer).substring(0, r);
                              if (receivedMessage.length() > 0) {
                                    TcpMessage tcpMessage1 = new Gson()
                                          .fromJson(receivedMessage, TcpMessage.class);
                                    String[] body = tcpMessage1.getBody().split(",");
                                    long c = Long.parseLong(body[0]);
                                    long d = Long.parseLong(body[1]);
                                    int s = Integer.parseInt(body[2]);
      
                                    Device device = new Device();
//                                device.setId(
//                                        Long.parseLong(
//                                               c +String.format("%3d0", d, Locale.US)
//                                        )
//                                );
                                    device.setDeviceId(d);
                                    device.setControllerId(c);
                                    device.setStatus(s);
                                    tcpMessage1
                                          .setBody(new Gson().toJson(device, Device.class));
                                    tcpMessage1.setBodyClass("Device");
      
                                    onResponse(tcpMessage1);
                                    System.out.println("FROM SERVER: " + receivedMessage);
                              }
                              clientSocket.close();
                        } catch (Exception e) {
                              System.out.println("ErrorFROM SERVER: " + e.getMessage());
                        }
                  }
            }).start();
      }
      
      public abstract void onResponse(TcpMessage response);
}