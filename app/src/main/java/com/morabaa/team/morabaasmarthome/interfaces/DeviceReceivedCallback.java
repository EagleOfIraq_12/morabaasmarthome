package com.morabaa.team.morabaasmarthome.interfaces;


import com.morabaa.team.morabaasmarthome.model.Device;

/**
 * Created by eagle on 3/28/2018.
 */
// checkIp data from activity to fragment
public interface DeviceReceivedCallback {
      void onDeviceReceived(Device device);
      
}
