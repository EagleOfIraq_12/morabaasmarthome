package com.morabaa.team.morabaasmarthome.activities;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static com.morabaa.team.morabaasmarthome.activities.MainActivity.db;
import static com.morabaa.team.morabaasmarthome.utils.SV.DEVICE_ID;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.morabaa.team.morabaasmarthome.R;
import com.morabaa.team.morabaasmarthome.adapters.RoomAdapter;
import com.morabaa.team.morabaasmarthome.model.Device;
import com.morabaa.team.morabaasmarthome.model.Relation;
import com.morabaa.team.morabaasmarthome.model.TcpMessage;
import com.morabaa.team.morabaasmarthome.tcp.TcpClient;
import com.morabaa.team.morabaasmarthome.tcp.TcpClientAbstract;
import com.morabaa.team.morabaasmarthome.tcp.TcpServer;
import com.morabaa.team.morabaasmarthome.utils.SV;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import java.util.ArrayList;
import java.util.List;

public class RoomActivity extends AppCompatActivity
      implements RoomAdapter.AdapterActivityBridge, /*DeviceReceivedCallback,*/
                 NavigationView.OnNavigationItemSelectedListener {
      
      public static final String VIEW_NAME_HEADER_TITLE = "detail:header:title";
      public static final String VIEW_NAME_HEADER_IMAGE = "detail:header:image";
      public TextView txtTemperature;
      public ImageView imgMotion;
      public TextView txtHumidity;
      public FragmentManager fragmentManager;
      
      LinearLayout layoutDoorStatus;
      TextView txtDoorStatus;
      ImageView imgDoorStatus;
      
      LinearLayout layoutMotionStatus;
      TextView txtMotionStatus;
      ImageView imgMotionStatus;
      
      long roomId; //TODO should be replaced with controllerId in some cases
      RecyclerView recyclerView;
      List<Device> devices;
      ColorMatrixColorFilter filter;
      
      RoomAdapter roomAdapter;
      LinearLayout layoutTurnAllOff;
      LinearLayout layoutTurnAllOn;
      TextView txtHeader;
      String roomName;
      ImageView imageRoom;
      TcpServer.OnTcpMessageReceivedListener onTcpMessageReceivedListener = new TcpServer.OnTcpMessageReceivedListener() {
            @Override
            public void onTcpMessageReceived(TcpMessage tcpMessage) {
                  String deviceString = tcpMessage.getBody();
                  final Device device = new Gson()
                        .fromJson(deviceString, Device.class);
                  
                  if (tcpMessage.getDeviceId().equals(DEVICE_ID) || true) {
                        switch (tcpMessage.getCommand()) {
                              case SV.TCP_COMMANDS.DEVICE.SET: {
                                    runOnUiThread(new Runnable() {
                                          @Override
                                          public void run() {
                                                getReceivedDevice(device);
                                          }
                                    });
                                    
                                    break;
                              }
                              case SV.TCP_COMMANDS.DEVICE.UPDATE: {
                                    db.deviceDuo().Update(device);
                                    Device currentDevice = db.deviceDuo()
                                          .getDeviceById(device.getId());
                                    System.out.println("updated:" +
                                          "\n" + new Gson().toJson(currentDevice));
                                    break;
                              }
                              case SV.TCP_COMMANDS.DEVICE.ADD: {

//                            db.deviceDuo().insert(device);
//                            Device currentDevice = db.deviceDuo()
//                                    .getDeviceById(device.getId());
//                            System.out.println("inserted:" +
//                                    "\n" + new Gson().toJson(currentDevice));
                                    break;
                              }
                        }
                  }
            }
      };
      private Vibrator vibrator;
      private long controllerId;
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_room);
            roomName = getIntent().getStringExtra("roomName");
            roomId = getIntent().getLongExtra("roomId", 0);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            
            // attach to current activity;
            ResideMenu resideMenu = new ResideMenu(this);
            resideMenu.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            resideMenu.attachToActivity(this);
            
            // create menu items;
            final String titles[] = {"اطفاء الكل", "تشغيل الكل", "اعدادات"};
            int icon[] = {R.drawable.power_off, R.drawable.power_on, R.drawable.settings};
            for (int i = 0; i < titles.length; i++) {
                  ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
                  final int finalI = i;
                  ImageView iv_icon = item.findViewById(R.id.iv_icon);
                  iv_icon.setPadding(5, 5, 5, 5);
                  TextView textView = item.findViewById(R.id.tv_title);
                  textView.setTextSize(14);
                  textView.setTextColor(getResources().getColor(R.color.colorAccent));
                  item.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              switch (finalI) {
                                    case (0): {
                                          vibrator.vibrate(70);
                                          for (Device d : devices) {
                                                final Device device = new Device();
                                                device.setStatus(1);
                                                device.setDeviceId(d.getDeviceId());
                                                device.setControllerId(
                                                      d.getControllerId());
                                                device.setStatus(0);
                                                if (d.getStatus() > 0) {
                                                      TcpClient.send(new TcpMessage() {{
                                                            setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                                                            setBody(new Gson()
                                                                  .toJson(device, Device.class));
                                                            setBodyClass("Device");
                                                      }});
                                                }
                                          }
                                          
                                          break;
                                    }
                                    case (1): {
                                          vibrator.vibrate(70);
                                          for (int i = devices.size() - 1; i >= 0; i--) {
                                                final Device device = new Device();
                                                device.setStatus(1);
                                                device.setDeviceId(devices.get(i).getDeviceId());
                                                device.setControllerId(
                                                      devices.get(i).getControllerId());
                                                
                                                if (devices.get(i).getStatus() < 1) {
                                                      TcpClient.send(new TcpMessage() {{
                                                            setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                                                            setBody(new Gson()
                                                                  .toJson(device, Device.class));
                                                            setBodyClass("Device");
                                                      }});
                                                }
                                          }
                                          break;
                                    }
                              }
                        }
                  });
                  resideMenu.addMenuItem(item,
                        ResideMenu.DIRECTION_RIGHT); // or  ResideMenu.DIRECTION_RIGHT
                  resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
            }

//            MainActivity.instant.setDeviceReceivedCallback(this);
//            TcpServer.getInstant()
//                  .addOnTcpMessageReceivedListener(onTcpMessageReceivedListener);
//            TcpServer.getInstant().setOnTcpMessageReceivedListener(onTcpMessageReceivedListener);

//            DrawerLayout drawer = findViewById(R.id.drawer_layout);
//            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//
            imageRoom = findViewById(R.id.imageRoom);
            imgDoorStatus = findViewById(R.id.imgDoorStatus);
            layoutDoorStatus = findViewById(R.id.layoutDoorStatus);
            txtDoorStatus = findViewById(R.id.txtDoorStatus);
            
            imgMotionStatus = findViewById(R.id.imgMotionStatus);
            layoutMotionStatus = findViewById(R.id.layoutMotionStatus);
            txtMotionStatus = findViewById(R.id.txtMotionStatus);
            
            txtHeader = findViewById(R.id.txtHeader);
            layoutTurnAllOn = findViewById(R.id.layoutTurnAllOn);
            layoutTurnAllOff = findViewById(R.id.layoutTurnAllOff);
            txtTemperature = findViewById(R.id.txtTemperature);
            txtHumidity = findViewById(R.id.txtHumidity);
            txtHeader.setText(roomName);
            recyclerView = findViewById(R.id.grid);
            
            try {
                  ViewCompat.setTransitionName(imageRoom, VIEW_NAME_HEADER_IMAGE);
                  ViewCompat.setTransitionName(txtHeader, VIEW_NAME_HEADER_TITLE);
            } catch (Exception ignored) {
            }
            txtTemperature.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        final Device device = new Device();
                        device.setControllerId(controllerId);
                        device.setDeviceId(30);
                        device.setStatus(0);
                        TcpClient.send(new TcpMessage() {{
                              setCommand(SV.TCP_COMMANDS.DEVICE.GET);
                              setBody(new Gson().toJson(device, Device.class));
                              setBodyClass("Device");
                        }});
                  }
            });
            controllerId = db.roomDuo().getRoomById(roomId).getControllerId();
            devices = new ArrayList<>();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            fragmentManager = getSupportFragmentManager();
            vibrator = (Vibrator) RoomActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
            
            roomAdapter = new RoomAdapter(
                  RoomActivity.this, devices, roomId);
            roomAdapter.setAdapterActivityBridge(this);
            recyclerView.setAdapter(roomAdapter);
            
            layoutTurnAllOff.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        vibrator.vibrate(70);
                        for (Device d : devices) {
                              final Device device = new Device();
                              device.setStatus(1);
                              device.setDeviceId(d.getDeviceId());
                              device.setControllerId(
                                    d.getControllerId());
                              device.setStatus(0);
                              if (d.getStatus() > 0) {
                                    
                                    new TcpClientAbstract() {
                                          @Override
                                          public void onResponse(TcpMessage tcpMessage) {
                                                String deviceString = tcpMessage.getBody();
                                                final Device device = new Gson()
                                                      .fromJson(deviceString, Device.class);
                                                runOnUiThread(new Runnable() {
                                                      @Override
                                                      public void run() {
                                                            roomAdapter.updateDevice(device);
                                                      }
                                                });
                                          }
                                    }.send(new TcpMessage() {{
                                          setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                                          setBody(new Gson().toJson(device, Device.class));
                                          setBodyClass("Device");
                                    }});
                              }
                        }
                  }
            });
            layoutTurnAllOn.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        vibrator.vibrate(70);
                        for (int i = devices.size() - 1; i >= 0; i--) {
                              final Device device = new Device();
                              device.setStatus(1);
                              device.setDeviceId(devices.get(i).getDeviceId());
                              device.setControllerId(
                                    devices.get(i).getControllerId());
                              
                              if (devices.get(i).getStatus() < 1) {
                                    new TcpClientAbstract() {
                                          @Override
                                          public void onResponse(TcpMessage tcpMessage) {
                                                String deviceString = tcpMessage.getBody();
                                                final Device device = new Gson()
                                                      .fromJson(deviceString, Device.class);
                                                runOnUiThread(new Runnable() {
                                                      @Override
                                                      public void run() {
                                                            roomAdapter.updateDevice(device);
                                                      }
                                                });
                                          }
                                    }.send(new TcpMessage() {{
                                          setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                                          setBody(new Gson().toJson(device, Device.class));
                                          setBodyClass("Device");
                                    }});
                              }
                        }
                  }
            });
            TcpServer.getInstant().addOnTcpMessageReceivedListener(
                  onTcpMessageReceivedListener);
//            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                  this, drawer, toolbar, R.string.navigation_drawer_open,
//                  R.string.navigation_drawer_close);
//            drawer.addDrawerListener(toggle);
//            toggle.syncState();
//
//            NavigationView navigationView = findViewById(R.id.nav_view);
//            navigationView.setNavigationItemSelectedListener(this);
            reloadDevices();
      
      }
      
      @Override
      public void onBackPressed() {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                  drawer.closeDrawer(GravityCompat.START);
            } else {
                  super.onBackPressed();
            }
      }
      
      @Override
      public void onResume() {
            super.onResume();
            if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
                  recyclerView.setLayoutManager(
                        new GridLayoutManager(RoomActivity.this, 2)
                  );
                  imageRoom.getLayoutParams().width = SV.ScreenDimensions.getWidth(this);
            } else {
                  recyclerView.setLayoutManager(
                        new GridLayoutManager(RoomActivity.this, 4)
                  );
            }
      }
      
      void reloadDevices() {
            devices = new ArrayList<>();
            try {
                  for (Device device : db.deviceDuo().DEVICES()) {
                        if (device.getRoomId() == roomId && (device.getType().equals("single")
                              || device.getType().equals("multiValue"))) {
                              device.setStatus(-1);
                              devices.add(device);
                        }
                  }
            } catch (Exception ignored) {
            }
            
            for (final Device device : devices) {
                  TcpClient.send(new TcpMessage() {{
                        setCommand(SV.TCP_COMMANDS.DEVICE.GET);
                        setBody(new Gson().toJson(device, Device.class));
                        setBodyClass("Device");
                  }});
            }
            {// get sensors
                  final Device device = new Device();
                  device.setControllerId(controllerId);
                  device.setDeviceId(30);
                  TcpClient.send(new TcpMessage() {{
                        setCommand(SV.TCP_COMMANDS.DEVICE.GET);
                        setBody(new Gson().toJson(device, Device.class));
                        setBodyClass("Device");
                  }});
                  device.setDeviceId(31);
                  TcpClient.send(new TcpMessage() {{
                        setCommand(SV.TCP_COMMANDS.DEVICE.GET);
                        setBody(new Gson().toJson(device, Device.class));
                        setBodyClass("Device");
                  }});
                  device.setDeviceId(32);
                  TcpClient.send(new TcpMessage() {{
                        setCommand(SV.TCP_COMMANDS.DEVICE.GET);
                        setBody(new Gson().toJson(device, Device.class));
                        setBodyClass("Device");
                  }});
            }
            roomAdapter.setDevices(devices);
            
      }
      
      public void getReceivedDevice(Device device) {
            switch ((int) device.getDeviceId()) {
                  case 30: {
                        if (
                              txtTemperature != null &&
                                    device.getStatus() > 5 &&
                                    device.getStatus() < 50
                              ) {
                              txtTemperature
                                    .setText(device.getStatus() + " ْ");
//                              if (device.getControllerId() == 117) {
//                                    MainActivity.instant.txtMainTemp
//                                          .setText(device.getStatus() + " ْ");
//                              }
                        }
                        break;
                  }
                  case 33: {
                        if (txtHumidity != null) {
                              txtHumidity
                                    .setText(device.getStatus() + " %");
//                              if (device.getControllerId() == 117) {
//                                    MainActivity.instant.txtMainHumidity
//                                          .setText(device.getStatus() + " %");
//                              }
                        }
                        break;
                  }
                  case 31: {
                        if (device.getStatus() == 0) {
                              imgDoorStatus.setImageResource(
                                    R.drawable.door_closed);
                              layoutDoorStatus.setBackgroundColor(
                                    getResources().getColor(R.color.colorPrimary));
                              txtDoorStatus.setText("الباب مغلق");
                              
                        } else {
                              imgDoorStatus.setImageResource(
                                    R.drawable.door_opened);
                              layoutDoorStatus.setBackgroundColor(
                                    getResources().getColor(R.color.colorAspen));
                              txtDoorStatus.setText("الباب مفتوح");
                        }
                        
                        break;
                  }
                  case 32: {
                        if (imgMotion != null) {
                              if (device.getStatus() == 0) {
                                    imgMotionStatus.setImageResource(
                                          R.drawable.movement_true);
                                    layoutMotionStatus.setBackgroundColor(
                                          getResources().getColor(R.color.colorAspen));
                                    txtMotionStatus.setText("توجد حركه");
                              } else {
                                    imgMotionStatus.setImageResource(
                                          R.drawable.movement_false);
                                    layoutMotionStatus.setBackgroundColor(
                                          getResources().getColor(R.color.colorPrimary));
                                    txtMotionStatus.setText("لا توجد حركه");
                              }
                        }
                        break;
                  }
                  default: {
                        device.setStatus(
                              device.getStatus());
                        roomAdapter.updateDeviceX(device);

//                      roomAdapter.setDevices(devices);
                        break;
                  }
            }
      }
      
      @Override
      public void onDeviceClicked(final int position) {
            vibrator.vibrate(30);
            
            roomAdapter.waitingDevice(devices.get(position));
            
//            getRelatives(devices.get(position).getId());
            if (devices.get(position).getType().equals("single")) {
                  int s;
                  if (devices.get(position).getStatus() == 0) {
                        s = 1;
                  } else {
                        s = 0;
                  }
                  final Device device = new Device();
                  device.setControllerId(devices.get(position).getControllerId());
                  device.setDeviceId(devices.get(position).getDeviceId());
                  device.setStatus(s);
                  
                  TcpClient.send(new TcpMessage() {{
                        setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                        setBody(new Gson().toJson(device, Device.class));
                        setBodyClass("Device");
                  }});
            } else {
                  AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                        RoomActivity.this);
                  LayoutInflater inflater = getLayoutInflater();
                  View dialogView = inflater
                        .inflate(R.layout.multi_value_dailog_layout, null);
                  
                  dialogBuilder.setView(dialogView);
                  final AlertDialog alertDialog = dialogBuilder.create();
                  dialogView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              alertDialog.dismiss();
                              
                        }
                  });
                  
                  LinearLayout up = dialogView.findViewById(R.id.up);
                  LinearLayout down = dialogView.findViewById(R.id.down);
                  
                  final TextView txtValue = dialogView.findViewById(R.id.txtValue);
                  final ProgressBar progressBar = dialogView.findViewById(R.id.circle_progress_bar);
                  new CountDownTimer(1000, 100) {
                        
                        public void onTick(
                              long millisUntilFinished) {
                              progressBar.setProgress(devices.get(position).getStatus() * 10);
                              txtValue.setText(devices.get(position).getStatus() * 10 + "");
                        }
                        
                        public void onFinish() {
                        
                        }
                  }.start();
                  up.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              final Device device = new Device();
                              device.setControllerId(roomId);
                              device.setDeviceId(devices.get(position).getDeviceId());
                              device.setStatus(
                                    Integer.parseInt(txtValue.getText().toString()) + 10);
                              device.setStatus(1);
                              
                              TcpClient.send(new TcpMessage() {{
                                    setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                                    setBody(new Gson().toJson(device, Device.class));
                                    setBodyClass("Device");
                              }});
                              new CountDownTimer(1000, 100) {
                                    
                                    public void onTick(
                                          long millisUntilFinished) {
                                          progressBar.setProgress(
                                                devices.get(position).getStatus() * 10);
                                          txtValue.setText(
                                                devices.get(position).getStatus() * 10 + "");
                                    }
                                    
                                    public void onFinish() {
                                    
                                    }
                              }.start();
                              
                        }
                  });
                  down.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              final Device device = new Device();
                              device.setControllerId(roomId);
                              device.setDeviceId(devices.get(position).getDeviceId());
                              device.setStatus(
                                    Integer.parseInt(txtValue.getText().toString()) - 10);
                              device.setStatus(0);
                              
                              TcpClient.send(new TcpMessage() {{
                                    setCommand(SV.TCP_COMMANDS.DEVICE.SET);
                                    setBody(new Gson().toJson(device, Device.class));
                                    setBodyClass("Device");
                              }});
                              new CountDownTimer(1000, 100) {
                                    
                                    public void onTick(
                                          long millisUntilFinished) {
                                          progressBar.setProgress(
                                                devices.get(position).getStatus() * 10);
                                          txtValue.setText(
                                                devices.get(position).getStatus() * 10 + "");
                                    }
                                    
                                    public void onFinish() {
                                    
                                    }
                              }.start();
                              
                        }
                  });
                  alertDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                  alertDialog.show();
                  alertDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(
                              android.graphics.Color.argb(100, 0, 0, 0))
                  );
                  alertDialog.getWindow()
                        .setLayout(SV.ScreenDimensions.getWidth(RoomActivity.this),
                              SV.ScreenDimensions.getHeight(RoomActivity.this));
            }
      }
      
      private void getRelatives(long id) {
            List<Relation> relations = db.relationDuo().getRelationsByDeviceId(id);
            for (Relation relation : relations) {
                  System.out.println("relation: " +
                        relation.getDeviceId() + "\t" +
                        relation.getRelationType() + "\t" +
                        relation.getToConnectDeviceId()
                  );
            }
      }
      
      @Override
      public void onDeviceLongClicked(int position) {
            vibrator.vibrate(70);
            Intent intent = new Intent(RoomActivity.this, DeviceSettingsActivity.class);
            intent.putExtra("deviceId", devices.get(position).getId());
            startActivity(intent);
      }

//      @Override
//      public void onDeviceReceived(Device device) {
//            getReceivedDevice(device);
//      }
      
      @Override
      protected void onDestroy() {
            super.onDestroy();
            TcpServer
                  .getInstant()
                  .removeOnTcpMessageReceivedListener(
                        onTcpMessageReceivedListener
                  );
      }
      
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            return false;
      }
      
}
